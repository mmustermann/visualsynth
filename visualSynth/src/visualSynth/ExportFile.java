package visualSynth;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import javax.swing.JFileChooser;

public class ExportFile {
	
	private Float[] samples;
	private File path;
	
	public ExportFile(Float[] samples2) {
		samples = samples2;
		saveDialog();
	}
	
	private void saveDialog() {
		System.out.println("save");
		final JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Specify a file to save");   

        int returnVal = fc.showSaveDialog(fc);
		 
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			path = fc.getSelectedFile();
			save();
		    System.out.println("Save as file: " + path.getAbsolutePath());
		}
	}
	
	// copied from the google!
	// http://computermusicblog.com/blog/2008/08/29/reading-and-writing-wav-files-in-java/
	// write out the wav file
	private boolean save()
	{		
		// sample rate
		int mySampleRate = 44100;
		// 8 bits = 8, 16 bits = 16, etc.
		short myBitsPerSample = 16;
		// Mono = 1, Stereo = 2, etc.
		int myChannels = 1;
		// 16 for PCM.  This is the size of the rest of the Subchunk which follows this number.
		int mySubChunk1Size = 16;
		// NumSamples * NumChannels * BitsPerSample/8
		int mySubchunk2Size = samples.length * myChannels *  myBitsPerSample / 8;
		// 36 + SubChunk2Size, or more precisely: 4 + (8 + SubChunk1Size) + (8 + SubChunk2Size) 
		// This is the size of the rest of the chunk following this number.  
		// This is the size of the entire file in bytes minus 8 bytes for the two fields not included in this count: ChunkID and ChunkSize.
		int myChunkSize = 4 + (8 + mySubChunk1Size) + (8 + mySubchunk2Size);
		// what is the audio format? 1 for PCM = Pulse Code Modulation
		short myFormat = 1;
		// SampleRate * NumChannels * BitsPerSample/8
		int myByteRate = mySampleRate * myChannels * myBitsPerSample / 8;
		// NumChannels * BitsPerSample/8
		short myBlockAlign = (short) (myChannels * myBitsPerSample / 8);
		
		// the actual data itself - just a long string of numbers
		byte[] myData = floatToByteArray(samples);
		// 40 - how big is this data chunk
		int myDataSize = myData.length;
		
		try
		{
			DataOutputStream outFile  = new DataOutputStream(new FileOutputStream(path.getAbsolutePath()));

			// write the wav file per the wav file format
			outFile.writeBytes("RIFF");					// 00 - RIFF
			outFile.write(intToByteArray((int)myChunkSize), 0, 4);		// 04 - how big is the rest of this file?
			outFile.writeBytes("WAVE");					// 08 - WAVE
			outFile.writeBytes("fmt ");					// 12 - fmt 
			outFile.write(intToByteArray((int)mySubChunk1Size), 0, 4);	// 16 - size of this chunk
			outFile.write(shortToByteArray((short)myFormat), 0, 2);		// 20 - what is the audio format? 1 for PCM = Pulse Code Modulation
			outFile.write(shortToByteArray((short)myChannels), 0, 2);	// 22 - mono or stereo? 1 or 2?  (or 5 or ???)
			outFile.write(intToByteArray((int)mySampleRate), 0, 4);		// 24 - samples per second (numbers per second)
			outFile.write(intToByteArray((int)myByteRate), 0, 4);		// 28 - bytes per second
			outFile.write(shortToByteArray((short)myBlockAlign), 0, 2);	// 32 - # of bytes in one sample, for all channels
			outFile.write(shortToByteArray((short)myBitsPerSample), 0, 2);	// 34 - how many bits in a sample(number)?  usually 16 or 24
			outFile.writeBytes("data");					// 36 - data
			outFile.write(intToByteArray((int)myDataSize), 0, 4);		// 40 - how big is this data chunk
			outFile.write(myData);						// 44 - the actual data itself - just a long string of numbers
			outFile.close();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return false;
		}

		return true;
	}	
    
    // converts 32 bit float to 16 bit byte
    private static byte[] floatToByteArray(Float[] samples2) {
        ByteBuffer buffer = ByteBuffer.allocate(2 * samples2.length);
        buffer.order(ByteOrder.LITTLE_ENDIAN);    
        for(int i = 0; i < samples2.length; i++) {
        	/*
        	float value = values[i];
        	float f = 0;
	        f = value * 2147483648f;
	        if( f > 2147483648f ) f = 2147483648f;
	        if( f < -2147483648f ) f = -2147483648f;
            buffer.putFloat(f);     
            */
        	short f = (short) (samples2[i] * 32767);
	        if( f > 32767 ) f = 32767;
	        if( f < -32767 ) f = -32767;
            buffer.putShort(f);    
        }
        return buffer.array();
    }

    // returns a byte array of length 4
    private static byte[] intToByteArray(int i)
    {
      byte[] b = new byte[4];
      b[0] = (byte) (i & 0x00FF);
      b[1] = (byte) ((i >> 8) & 0x000000FF);
      b[2] = (byte) ((i >> 16) & 0x000000FF);

      b[3] = (byte) ((i >> 24) & 0x000000FF);
      return b;
    }

    // convert a short to a byte array
    private static byte[] shortToByteArray(short data)
    {

      return new byte[]{(byte)(data & 0xff),(byte)((data >>> 8) & 0xff)};
    }
}
