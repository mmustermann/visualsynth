package visualSynth;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class FileLoader {
	
	public void fileLoader() {
		
	}
	
	public BufferedImage loadfile(File file) {	
		BufferedImage img = null;
		try {
		    img = ImageIO.read(file);
		} catch (IOException e) {
			//do stuff?
		}
		
		BufferedImage convertedImg = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
		convertedImg.getGraphics().drawImage(img, 0, 0, null);
		
		int[] data = ((DataBufferInt) convertedImg.getRaster().getDataBuffer()).getData();
		
		float avgBrightness = getAvgAmplitude(data);
		data = reduceRange(data, avgBrightness);
		
		return convertedImg;
	}
	
	private float getAvgAmplitude(int[] data) {
		float total = 0;
		for(int rgb : data) {
			int red = (rgb>>16)&255;
			int green = (rgb>>8)&255;
			int blue = (rgb)&255;		
			total += ((red + green + blue) / 3);
		}
		return (total / data.length);
	}
	
	private int[] reduceRange(int[] data, float f) {
		for(int i = 0; i < data.length; i++) {
			int rgb = data[i];
			int red = (rgb>>16)&255;
			int green = (rgb>>8)&255;
			int blue = (rgb)&255;		
			float brightness = ((red + green + blue) / 3);	
			if(brightness < f) {
				data[i] = Color.BLACK.getRGB();
			}
		}
		return data;	
	}
}
