package visualSynth;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.ArrayList;
import javax.swing.JComponent;


public class ImgView extends JComponent implements MouseListener, MouseMotionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6268937805589921791L;
	
	private BufferedImage image;
	// array to store image data
	private int[] imageData;
	// array to store amplitudes
	private float[] amplitudes;
	
	private Rectangle selection;
	private int brushSize;
	private int width;
	private int height;
	private Color paintColor;
	private Point startPoint;
	private ArrayList<Point> points;
	Graphics2D g2d;
	
	private boolean paint;
	
	public ImgView(BufferedImage img) {
		image = img;
		width = image.getWidth();
		height = image.getHeight();	
		brushSize = 2;
		setBounds(0, 0, width, height);
		paintColor = new Color(255, 255, 255, 255);
		
		// get image data
		imageData = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
		// get amplitudes
		amplitudes = setAmplitudes();
		
		g2d = image.createGraphics();
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		points = new ArrayList<Point>();
		
		addMouseListener(this);	
		addMouseMotionListener(this);
	}

	public boolean gotSelection() {
		return selection == null ? false : true;
	}
	
	public Rectangle getSelection() {
		return selection;
	}
	
	public void clearSelection() {
		selection = null;
	}
	
	public void setPaint() {
		if(!paint) {
			paint = true;
		} else {
			paint = false;
		}
	}
	
	public void setBrushSize(int b) {
		brushSize = b;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setAlpha(float a) {
		paintColor = new Color((int) paintColor.getRed(), (int) paintColor.getGreen(), (int) paintColor.getBlue(), (int) (a * 255));
	}
	
	public void setColor(Color c) {
		paintColor = c;
	}
	
	public void update() {
		repaint();
	}
		
	// returns array with amplitudes
	public float[] getAmplitudes() {
		return amplitudes;
	}
		
	public void cropImage() {
		// create new buffered image
		BufferedImage newImage = new BufferedImage(selection.width, selection.height, BufferedImage.TYPE_INT_RGB);
		// involke graphic object
		Graphics2D tg = (Graphics2D)newImage.getGraphics();
		// copy current selection on new image
	    tg.drawImage(image.getSubimage(selection.x, selection.y, selection.width, selection.height), 0, 0, null);
	    tg.dispose();
	    // set new image
	    image = newImage;	
		width = image.getWidth();
		height = image.getHeight();
		
		// get amplitudes
		amplitudes = setAmplitudes();
		
		setBounds(0,0,image.getWidth(),image.getHeight());
		repaint();
	}	
	
	private void paint(Line2D.Float line) {	
		g2d.setColor(paintColor);
		g2d.setStroke(new BasicStroke(brushSize));
		g2d.drawLine((int) line.x1, (int) line.y1, (int) line.x2, (int) line.y2);
		
		Point start = new Point();
		start.x = (int) (line.x1 < line.x2 ? line.x1 - 1 : line.x2 - 1);
		start.y = (int) (line.y1 < line.y2 ? line.y1 - 1 : line.y2 - 1);		

		Point end = new Point();
		end.x = (int) (line.x1 > line.x2 ? line.x1 + 2 : line.x2 + 2);
		end.y = (int) (line.y1 > line.y2 ? line.y1 + 2 : line.y2 + 2);	

		Rectangle updateArea = new Rectangle(start.x, start.y, end.x, end.y);
		updateAmplitude(updateArea);
		
		repaint();
	}
	
	// draw gradient in current selection
	public void drawGradient() {
		// do we have a selection?
		if(selection != null) {
			// store original alpha
			int alphaOrg = paintColor.getAlpha();
			Color col;
			float alpha = 0;
			
			int start = selection.x;
			int end = start + selection.width;
			// dummy at 50%
			int peak = (int) (start + (end - start) * 0.5);
			
			for(int i = start; i < end; i++) {

				if(i < peak) {
					// increase alpha until peak
					alpha = ((i - start) / (float) (peak - start)); 
					
				} else {
					// reduce alpha
					alpha = ((end - i) / (float)  (end - peak)); 				
				}
				// set alpha of new color
				col = new Color((int) paintColor.getRed(), (int) paintColor.getGreen(), (int) paintColor.getBlue(), (int) (alpha * alphaOrg));
				
				// draw				
				g2d.setColor(col);
				// g2d.setStroke(new BasicStroke(brushSize));
				g2d.drawLine(i, selection.y, i, (selection.y + selection.height));
				
				repaint();
				// System.out.println(alpha);
			}
			Rectangle updateArea = new Rectangle(start, selection.y, end, (selection.y + selection.height));
			updateAmplitude(updateArea);
		}
	}
	
	private void select(Point p) {
		if(startPoint == null) {
			startPoint = new Point(p.x, p.y);
			selection = new Rectangle(startPoint.x, startPoint.y, 0, 0);
		} else {	
			int width = p.x - startPoint.x;
			int height = p.y - startPoint.y;
			
			if(width > 0) {
				selection.width = width;				
			} else {
				selection.x = p.x;
				selection.width = (int) Math.abs(startPoint.getX() - p.x);
			}		
			if(height > 0) {
				selection.height = height;
			} else {
				selection.y = p.y;
				selection.height = (int) Math.abs(startPoint.getY() - p.y);
			}
			repaint();
		}
	}

	// returns array with all amplitudes from the image
	private float[] setAmplitudes() {	
		// Array for amplitude
		float[] ampNew = new float[width * height];
		int counter = 0;
		for(int i = 0; i < width; i++) {
			for(int f = 0; f < height; f++) {
				ampNew[counter] = (float) getAmplitude(imageData[(f * width) + i]);
				counter++;
			}
		}	
		return ampNew;
	}
	
	// convert rgb int to amplitude
	private float getAmplitude(int rgb) {
		int red = (rgb>>16)&255;
		int green = (rgb>>8)&255;
		int blue = (rgb)&255;		
		float brightness = ((red + green + blue) / 3);	
		return (brightness / 255);
	}
	
	// update all amplitudes values in that area
	private void updateAmplitude(Rectangle updateArea) {	
		// update each amplitude in update area
		for(int x = updateArea.x; x < updateArea.width; x++) {
			if(x > 0 && x < width) {
				for(int y = updateArea.y; y < updateArea.height; y++) {
					if(y > 0 && y < height) {
						// get amplitude
						float amplitude = (float) getAmplitude(imageData[(y * width) + x]);			
						// set amplitude
						amplitudes[(x * height) + y] = amplitude;	
					}
				}	
			}
		}		
	}

	@Override
	public void paintComponent(Graphics g) {
		setForeground(Color.BLACK);
	    g.drawImage(image, 0, 0, width, height, null);	    
	    if(selection != null) {
	    	g.setColor(Color.red);
	    	g.drawRect(selection.x, selection.y, selection.width, selection.height);
	    }

	    Color col = new Color(0, 255, 0, 50);
	    g.setColor(col);
	    
	    g.drawLine(0, (int) (height * 0.25), width, (int) (height * 0.25));
	    g.drawLine(0, (int) (height * 0.5), width, (int) (height * 0.5));
	    g.drawLine(0, (int) (height * 0.75), width, (int) (height * 0.75));
	    
	    g.drawLine((int) (width * 0.25), 0, (int) (width * 0.25), height);
	    g.drawLine((int) (width * 0.5), 0, (int) (width * 0.5), height);
	    g.drawLine((int) (width * 0.75), 0, (int) (width * 0.75), height);
	}
	
	@Override	
	public Dimension getPreferredSize() {
		return new Dimension(width, height);	
	}

	@Override	
	public Dimension getMinimumSize() {
		return new Dimension(width, height);	
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		if(paint) {
			paint(new Line2D.Float(e.getX(), e.getY(), e.getX(), e.getY()));
		} else {
			selection = null;	
			startPoint = null;
			repaint();
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		points.clear();		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		if(paint) {
			Point p = new Point();
			p.x = e.getX() > 0 ? e.getX() : 0;
			p.x = e.getX() < width ? p.x : width;
			
			p.y = e.getY() > 0 ? e.getY() : 0;
			p.y = e.getY() < height ? p.y : height;
			
			points.add(p);
			
			if(points.size() > 10) {
				Point lastPoint = new Point(points.get(3).x, points.get(3).y);						
				Line2D.Float line = new Line2D.Float(points.get(0).x, points.get(0).y, points.get(3).x, points.get(3).y);
				points.clear();
				points.add(lastPoint);
				paint(line);
			}
		} else {					
			Point p = new Point();
			p.x = e.getX() > 0 ? e.getX() : 0;
			p.x = e.getX() < width ? p.x : width;
			
			p.y = e.getY() > 0 ? e.getY() : 0;
			p.y = e.getY() < height ? p.y : height;
						
			select(p);
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
