package visualSynth;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

public final class SamplerEntry extends JPanel implements ActionListener, DragGestureListener, Transferable {

	private static final long serialVersionUID = 5115497035852867003L;
	
	protected static DataFlavor flavor = new DataFlavor(SoundController.class, "A SamplerEntry Object");
	protected static DataFlavor[] supportedFlavors = {flavor};
	
	private SoundController soundController;
	private SamplerView samplerView;
	
	private String name;
	private int id;

	private JLabel label;
	public JToggleButton btnPlay;
	public JToggleButton btnLoop;
	
	private int width;
	private int height;
	
	public SamplerEntry(int id, String name, BufferedImage img) {	
		this.id = id;
		this.name = name;
			    
	    width = 200;
	    height = 40;
	    
	    // ---------- for drag and drop ----------//
	    // Use the default DragSource
	    DragSource dragSource = DragSource.getDefaultDragSource();
	    // Create a DragGestureRecognizer and register as the listener
	    dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_LINK, this);

	    // setup soundController
		soundController = new SoundController(this);
		
    	// setup sampler view
    	samplerView = new SamplerView(img, soundController, name);	
		
		setBackground(Color.LIGHT_GRAY);
		
		label = new JLabel(this.name);
		
		btnPlay = new JToggleButton("P");
		btnPlay.setPreferredSize(new Dimension(50, 40));
		btnPlay.addActionListener(this);
		
		btnLoop = new JToggleButton("L");
		btnLoop.setPreferredSize(new Dimension(50, 40));
		btnLoop.addActionListener(this);
		
		add(label);
		add(btnPlay);
		add(btnLoop);	
	}
	
	public String getName() {
		return name;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public SoundController getSoundcontroller() {
		return soundController;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {	
		if(e.getSource() == btnPlay){
			soundController.play();
		}		
		if(e.getSource() == btnLoop){
			soundController.loop();
		}	
	}

	@Override	
	// Implementation of DragGestureListener interface.
	public void dragGestureRecognized(DragGestureEvent event) {	
		// System.out.println("drag event started");	
    	Cursor cursor = null;
    	cursor = DragSource.DefaultCopyDrop; 
    	event.startDrag(cursor, this);
	}

	@Override
	public boolean isDataFlavorSupported(DataFlavor flavor) {
		if(flavor.equals(flavor)) {		
			// System.out.println("data flavor supported");
			return true;
		}
		return false;
	}

	@Override
	public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {	
		// System.out.println("get transfer data");
		return soundController;
	}

	@Override
	public DataFlavor[] getTransferDataFlavors() {
		// System.out.println("get data flavors");
		return supportedFlavors;
	}
		
	@Override	
	public Dimension getPreferredSize() {
		return new Dimension(width, height);	
	}

	@Override	
	public Dimension getMinimumSize() {
		return new Dimension(width, height);	
	}
}
