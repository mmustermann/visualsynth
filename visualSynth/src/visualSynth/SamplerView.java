package visualSynth;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.util.Hashtable;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public final class SamplerView implements ActionListener, ChangeListener{
	
	private SoundController soundController;
	
	public JFrame interfaceFrame;		
	public ImgView imageView;	
	public VisView visView;
	
	private JToggleButton btnSin;
	private JToggleButton btnSaw;	
	private JToggleButton btnSquare;	
	public JToggleButton btnPlay;
	public JToggleButton btnLoop;
	public JButton btnExport;
	
	private JSlider sliderGain;
	private JSlider sliderVolume;
	private JSlider sliderSpeed;
	private JSlider sliderPitch;
	private JSlider sliderDelay;
	private JSlider sliderSmooth;
	
	private JToggleButton btnPaint;	
	private JToggleButton btnBlack;
	private JToggleButton btnWhite;
	private JButton btnGradient;
	private JButton btnCrop;

	private JSlider sliderBrushSize;
	private JSlider sliderAlpha;
	
	private WindowAdapter windowAdapter;
	
	public SamplerView(BufferedImage image, SoundController soundCont, String name) {	
		
		soundController = soundCont;
		
        windowAdapter = new WindowAdapter()
        {
            public void windowClosing(WindowEvent we)
            {
            	soundController.stopPlaying();
            	Main.startScreen.removeEntry(soundController);
            	interfaceFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            }
        };       
		
		// setup frame
		interfaceFrame = new JFrame(name);
    	interfaceFrame.setLayout(new java.awt.FlowLayout());
    	interfaceFrame.setResizable(false);
		interfaceFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        interfaceFrame.addWindowListener(windowAdapter);

        // setup panel to display image and graph
    	JPanel panelImage = new JPanel();
    	panelImage.setLayout(new BoxLayout(panelImage, BoxLayout.Y_AXIS));
    	interfaceFrame.add(panelImage);
    	
    	// setup panel for sound controls
    	JPanel panelControl = new JPanel();
        panelControl.setLayout(new java.awt.FlowLayout());
    	panelControl.setPreferredSize(new Dimension(200, image.getHeight() +200));
    	
        JScrollPane scrPane = new JScrollPane(panelControl);
        scrPane.setPreferredSize(new Dimension(220, image.getHeight()));

    	// setup panel for paint controls
    	JPanel panelPaint = new JPanel();
    	panelPaint.setLayout(new java.awt.FlowLayout());
    	panelPaint.setPreferredSize(new Dimension(200, image.getHeight() +120));
        
    	// setup tabs
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Sound", null, scrPane, "Sound controls");
        tabbedPane.addTab("Paint", null, panelPaint,"Paint controls");
        interfaceFrame.add(tabbedPane);       
    	
		btnSin = new JToggleButton("Sin");
		btnSin.setPreferredSize(new Dimension(60, 40));
		btnSin.addActionListener(this);
		  	
		btnSaw = new JToggleButton("Saw");
		btnSaw.setPreferredSize(new Dimension(60, 40));
		btnSaw.addActionListener(this);
		
		btnSquare = new JToggleButton("Sq");
		btnSquare.setPreferredSize(new Dimension(60, 40));
		btnSquare.addActionListener(this);
		
		btnPlay = new JToggleButton("Play");
		btnPlay.setPreferredSize(new Dimension(200, 40));
		btnPlay.addActionListener(this);
		
		btnLoop = new JToggleButton("Loop");
		btnLoop.setPreferredSize(new Dimension(200, 40));
		btnLoop.addActionListener(this);
		
		btnExport = new JButton("Export");
		btnExport.setPreferredSize(new Dimension(200, 40));
		btnExport.addActionListener(this);
		
		sliderGain = new JSlider(JSlider.HORIZONTAL, 0, 100, 50);
		sliderGain.setMinorTickSpacing(10);
		sliderGain.setMajorTickSpacing(50);
		sliderGain.setPaintTicks(true);
		sliderGain.setPaintLabels(true);
		Hashtable<Integer, JLabel> labelGain = new Hashtable<Integer, JLabel>();
		labelGain.put( new Integer( 0 ), new JLabel("0.0") );
		labelGain.put( new Integer( 50 ), new JLabel("0.25") );
		labelGain.put( new Integer( 100 ), new JLabel("0.5") );
		sliderGain.setLabelTable(labelGain);	
		sliderGain.addChangeListener(this);
		
		sliderVolume = new JSlider(JSlider.HORIZONTAL, 0, 100, 67);
		sliderVolume.setMinorTickSpacing(10);
		sliderVolume.setMajorTickSpacing(50);
		sliderVolume.setPaintTicks(true);
		sliderVolume.setPaintLabels(true);
		Hashtable<Integer, JLabel> labelTableVol = new Hashtable<Integer, JLabel>();
		labelTableVol.put( new Integer( 0 ), new JLabel("0.0") );
		labelTableVol.put( new Integer( 50 ), new JLabel("0.5") );
		labelTableVol.put( new Integer( 100 ), new JLabel("1.0") );
		sliderVolume.setLabelTable(labelTableVol);	
		sliderVolume.addChangeListener(this);
		
		sliderSpeed = new JSlider(JSlider.HORIZONTAL, 0, 100, 62);
		sliderSpeed.setMinorTickSpacing(10);
		sliderSpeed.setMajorTickSpacing(50);
		sliderSpeed.setPaintTicks(true);
		sliderSpeed.setPaintLabels(true);
		Hashtable<Integer, JLabel> labelTableSpeed = new Hashtable<Integer, JLabel>();
		labelTableSpeed.put( new Integer( 0 ), new JLabel("x0.1") );
		labelTableSpeed.put( new Integer( 62 ), new JLabel("x1.0") );
		labelTableSpeed.put( new Integer( 100 ), new JLabel("x4.0") );
		sliderSpeed.setLabelTable( labelTableSpeed );
		sliderSpeed.addChangeListener(this);
			
		sliderPitch = new JSlider(JSlider.HORIZONTAL, 0, 100, 50);
		sliderPitch.setMinorTickSpacing(10);
		sliderPitch.setMajorTickSpacing(50);
		sliderPitch.setPaintTicks(true);
		sliderPitch.setPaintLabels(true);
		Hashtable<Integer, JLabel> labelTablePitch = new Hashtable<Integer, JLabel>();
		labelTablePitch.put(new Integer(0), new JLabel("-1.0"));
		labelTablePitch.put(new Integer(50), new JLabel("0.0"));
		labelTablePitch.put(new Integer(100), new JLabel("+1.0"));
		sliderPitch.setLabelTable( labelTablePitch );	
		sliderPitch.addChangeListener(this);
		
		sliderDelay = new JSlider(JSlider.HORIZONTAL, 0, 1000, 0);
		sliderDelay.setMinorTickSpacing(50);
		sliderDelay.setMajorTickSpacing(250);
		sliderDelay.setPaintTicks(true);
		sliderDelay.setPaintLabels(true);
		sliderDelay.setLabelTable(sliderDelay.createStandardLabels(250));
		sliderDelay.addChangeListener(this);
				
		sliderSmooth = new JSlider(JSlider.HORIZONTAL, 0, 20, 0);
		sliderSmooth.setMinorTickSpacing(2);
		sliderSmooth.setMajorTickSpacing(5);
		sliderSmooth.setPaintTicks(true);
		sliderSmooth.setPaintLabels(true);
		sliderSmooth.setLabelTable(sliderSmooth.createStandardLabels(5));
		sliderSmooth.addChangeListener(this);
		
		btnPaint = new JToggleButton("Paint");
		btnPaint.setPreferredSize(new Dimension(200, 40));
		btnPaint.addActionListener(this);
		
		btnBlack = new JToggleButton("Black");
		btnBlack.setPreferredSize(new Dimension(95, 40));
		btnBlack.addActionListener(this);
		
		btnWhite = new JToggleButton("White");
		btnWhite.setPreferredSize(new Dimension(95, 40));
		btnWhite.addActionListener(this);	
		
		btnGradient = new JButton("Draw Gradient");
		btnGradient.setPreferredSize(new Dimension(200, 40));
		btnGradient.addActionListener(this);
		
		btnCrop = new JButton("Crop");
		btnCrop.setPreferredSize(new Dimension(200, 40));
		btnCrop.addActionListener(this);
		
		sliderBrushSize = new JSlider(JSlider.HORIZONTAL, 1, 10, 2);
		sliderBrushSize.setMinorTickSpacing(1);
		sliderBrushSize.setMajorTickSpacing(2);
		sliderBrushSize.setPaintTicks(true);
		sliderBrushSize.setPaintLabels(true);
		sliderBrushSize.setLabelTable(sliderBrushSize.createStandardLabels(2));
		sliderBrushSize.addChangeListener(this);
		
		sliderAlpha = new JSlider(JSlider.HORIZONTAL, 0, 100, 100);
		sliderAlpha.setMinorTickSpacing(10);
		sliderAlpha.setMajorTickSpacing(50);
		sliderAlpha.setPaintTicks(true);
		sliderAlpha.setPaintLabels(true);
		Hashtable<Integer, JLabel> labelTable2 = new Hashtable<Integer, JLabel>();
		labelTable2.put( new Integer( 0 ), new JLabel("0.0") );
		labelTable2.put( new Integer( 50 ), new JLabel("0.5") );
		labelTable2.put( new Integer( 100 ), new JLabel("1.0") );
		sliderAlpha.setLabelTable( labelTable2 );
		sliderAlpha.addChangeListener(this);
		
    	// image view
    	imageView = new ImgView(image);
    	visView = new VisView(image.getWidth(), 100);
		
		// setup sound control
		panelControl.add(btnSin);
		btnSin.setSelected(true);
		panelControl.add(btnSaw);
		panelControl.add(btnSquare);
		panelControl.add(btnPlay);
		panelControl.add(btnLoop);
		panelControl.add(new JLabel("Gain"));		
		panelControl.add(sliderGain);			
		panelControl.add(new JLabel("Volume"));		
		panelControl.add(sliderVolume);		
		panelControl.add(new JLabel("Speed"));		
		panelControl.add(sliderSpeed);
		panelControl.add(new JLabel("Pitch"));		
		panelControl.add(sliderPitch);	
		panelControl.add(new JLabel("Delay"));		
		panelControl.add(sliderDelay);	
		panelControl.add(new JLabel("Smoothing"));		
		panelControl.add(sliderSmooth);	
		panelControl.add(btnExport);
		panelControl.add(btnExport);
		
		// setup paint control
		panelPaint.add(btnPaint);
		panelPaint.add(btnBlack);	
		panelPaint.add(btnWhite);
		btnWhite.setSelected(true);
		panelPaint.add(new JLabel("Brush Size"));
		panelPaint.add(sliderBrushSize);	
		panelPaint.add(new JLabel("Alpha"));
		panelPaint.add(sliderAlpha);	
		panelPaint.add(btnGradient);
		panelPaint.add(btnCrop);		
		
    	// setup image view
		panelImage.add(imageView); 	
		// spacing
		panelImage.add(Box.createRigidArea(new Dimension(0,10)));
		panelImage.add(visView);	    	
		
		// handle to sound controller		
		soundController.setSamplerView(this);
		soundController.initSoundSampler();
			
		// set visible
    	interfaceFrame.pack();
		interfaceFrame.setVisible(true);	
	}

	public void stateChanged(ChangeEvent e) {
	    JSlider source = (JSlider)e.getSource();
	    
	    if(e.getSource() == sliderGain) {
		    if (!source.getValueIsAdjusting()) {
		        soundController.setGain(source.getValue());
		    }
	    }
	    if(e.getSource() == sliderVolume) {
		    if (!source.getValueIsAdjusting()) {
		        soundController.setVolume(source.getValue());
		    }
	    }    
	    if(e.getSource() == sliderSpeed) {
		    if (!source.getValueIsAdjusting()) {
		        soundController.setSpeed(source.getValue());
		    }
	    }
	    if(e.getSource() == sliderDelay) {
		    if (!source.getValueIsAdjusting()) {
		        soundController.setDelay((int) source.getValue());
		    }
	    }
	    if(e.getSource() == sliderSmooth) {
		    if (!source.getValueIsAdjusting()) {
		        soundController.setSmoothing((int) source.getValue());
		    }
	    }
	    if(e.getSource() == sliderPitch) {
		    if (!source.getValueIsAdjusting()) {
		        soundController.setPitch(source.getValue());
		    }
	    }
	    if(e.getSource() == sliderBrushSize) {
		    if (!source.getValueIsAdjusting()) {
		        imageView.setBrushSize((int) source.getValue());
		    }
	    }
	    if(e.getSource() == sliderAlpha) {
		    if (!source.getValueIsAdjusting()) {
		        imageView.setAlpha((float) (source.getValue() * 0.01));
		    }
	    }
	}	    

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnSin){		
			soundController.setMode("SIN");
			btnSaw.setSelected(false);
			btnSquare.setSelected(false);
		}
		if(e.getSource() == btnSaw){	
			soundController.setMode("SAW");
			btnSin.setSelected(false);
			btnSquare.setSelected(false);
		}
		if(e.getSource() == btnSquare){		
			soundController.setMode("SQUARE");
			btnSin.setSelected(false);		
			btnSaw.setSelected(false);
		}
		
		if(e.getSource() == btnPlay){
			soundController.play();
		}	
		
		if(e.getSource() == btnLoop){
			soundController.loop();
		}	
		
		
		if(e.getSource() == btnExport){
			soundController.export();
		}	
		
		if(e.getSource() == btnPaint){
			imageView.setPaint();
		}	
		
		if(e.getSource() == btnBlack){
			imageView.setColor(new Color(0, 0, 0, 255));
	        imageView.setAlpha((float) (sliderAlpha.getValue() * 0.01));
			btnWhite.setSelected(false);
		}	
		
		if(e.getSource() == btnWhite){
			imageView.setColor(new Color(255, 255, 255, 255));
	        imageView.setAlpha((float) (sliderAlpha.getValue() * 0.01));
			btnBlack.setSelected(false);
		}	
		
		if(e.getSource() == btnGradient){
			imageView.drawGradient();
		}
		
		if(e.getSource() == btnCrop){
			soundController.crop();
		}
	}
}
