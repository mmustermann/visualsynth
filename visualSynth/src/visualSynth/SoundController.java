package visualSynth;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

public final class SoundController {
	
	private SoundSampler soundSampler;
	private SamplerView samplerView;
	private SamplerEntry samplerEntry;
	private BufferedImage vis;

	private boolean playing;
	private boolean looping;
	
	private static int sampleRate = 44100;
	private int width;
	private int origWidth;
	
	// for timeline
	private double duration;
	
	public SoundController(SamplerEntry samplerEntry) {
		this.samplerEntry = samplerEntry;
	}	

	// returns duration
	public double getDuration() {
		return duration;
	}
	
	public BufferedImage getVis() {
		return vis;
	}
		
	// initiate sound sampler
	public void initSoundSampler() {		
		playing = false;
		looping = false;		
		
		// set stuff
		vis = samplerView.visView.getImage();	
		width = samplerView.imageView.getWidth();
		// need to store original width for cropping
		origWidth = width;	
		
		// need to set all this for sound sampler to work
		soundSampler = new SoundSampler(this);		
		soundSampler.freqHigh = 4000;	
		soundSampler.freqMid = 1000;
		soundSampler.freqLow = 30;
		soundSampler.gain = 0.25f;
		soundSampler.volume = 0.5f;
		soundSampler.delayLengthNew = 0;
		soundSampler.smoothLengthNew = 0;
		soundSampler.decay = 0.4f;
		soundSampler.mode = soundSampler.SIN;
		soundSampler.dynamicAmpControl = true;
		soundSampler.valueChanged = true;
		soundSampler.loop = false;
		soundSampler.sampleRate = sampleRate;
		soundSampler.samplesPerFrameNew = (int) (sampleRate / origWidth);		
		soundSampler.imgWidth = width;
		soundSampler.imgHeight = samplerView.imageView.getHeight();		
		soundSampler.amplitudes = samplerView.imageView.getAmplitudes();		
		soundSampler.visData = ((DataBufferInt) vis.getRaster().getDataBuffer()).getData();
		soundSampler.visHeight = vis.getHeight();	
		soundSampler.init();
			
		// set duration
		duration = 1;
	}
	
	// set samplerView to access buttons
	public void setSamplerView(SamplerView samplerView) {
		this.samplerView = samplerView;
	}

	// start playing
	public void play() {
		if(!playing) {
			playing = true;
			soundSampler.start();
			// set button state
			samplerView.btnPlay.setSelected(true);
			samplerEntry.btnPlay.setSelected(true);
		} else {
			stopPlaying();
		}
	}
	
	// stop sound sampler
	public void stopPlaying() {
		// set button state		
		samplerView.btnPlay.setSelected(false);
		samplerEntry.btnPlay.setSelected(false);
		soundSampler.stop();
		playing = false;
	}

	// start | stop looping
	public void loop() {
		if(!looping) {
			looping = true;
			// set button state			
			samplerView.btnLoop.setSelected(true);
			samplerEntry.btnLoop.setSelected(true);
		} else {
			looping = false;
			// set button state			
			samplerView.btnLoop.setSelected(false);
			samplerEntry.btnLoop.setSelected(false);
		}
		soundSampler.loop = looping;
	}
	
	// set wave generator mode
	public void setMode(String mode) {
		if(mode == "SIN") {
			soundSampler.mode = soundSampler.SIN;
		} else if (mode == "SAW") {
			soundSampler.mode = soundSampler.SAW;			
		} else if (mode == "SQUARE") {
			soundSampler.mode = soundSampler.SQUARE;			
		}
		soundSampler.valueChanged = true;
	}

	// uhmmm do stuff 
	public void setPitch(float p) {
		int freqHigh;
		int freqLow;
		// adjust range to [-1 .. 1]
		float pitch = (float) ((p * 0.02) -1);
		// maximum half range of frequencies
		int offset = (int) ((soundSampler.freqHigh - soundSampler.freqLow) * pitch * 0.5);
		// lower frequency
		if(pitch < 0) {
			freqHigh = soundSampler.freqHigh + offset;
			freqLow = soundSampler.freqLow;	
		//higher frequency			
		} else {
			freqHigh = soundSampler.freqHigh;
			freqLow = soundSampler.freqLow + offset;
		}
		// update frequencies
		soundSampler.frequencies = soundSampler.getFrequencies(freqHigh, freqLow);
		soundSampler.valueChanged = true;
	}
		
	// set playback speed
	public void setSpeed(float s) {		

		//float min = 0.25;
		//float max = 10;
		duration = (float) ((0.0011 * Math.pow(s, 2)) - (0.2075 * s) + 10);
		// round
		duration = (float) (Math.round(duration * 4) / 4f);
		System.out.println(s + " : " + duration);
		
		soundSampler.samplesPerFrameNew = (int) (sampleRate * duration / origWidth);
		soundSampler.valueChanged = true;			
		
		/*
		float min = 0.25f;
		float max = 4f;
		// adjust range to [min .. max}
		float speed = (min + (1 - s * 0.01f) * (max - min));	
	
		soundSampler.samplesPerFrameNew = (int) (sampleRate / origWidth * speed);	
		soundSampler.valueChanged = true;	
			
		// set duration
		duration = width * soundSampler.samplesPerFrameNew / (float) sampleRate * 1000;
		*/
	}
	
	// set delay
	public void setDelay(int delay) {
		soundSampler.delayLengthNew = delay;
		soundSampler.valueChanged = true;
	}
	
	// set echo
	public void setSmoothing(int smoothLength) {
		soundSampler.smoothLengthNew = smoothLength;
		soundSampler.valueChanged = true;
	}
	
	// set volume
	public void setVolume(float v) {		
		// adjust range to [0 .. 1]
		soundSampler.volume = (float) (v * 0.01);
		//soundSampler.valueChanged = true;
	}
	
	// set gain
	public void setGain(float g) {		
		// adjust range to [0 .. 0.5]
		soundSampler.gain = (float) (g * 0.005);
		// soundSampler.valueChanged = true;
	}
	
	// redraw vis view
	public void updateVis() {
		samplerView.visView.update();
	}
	
	// crop the used image to the selected part
	public void crop() {
		// do we have a selection?
		if(samplerView.imageView.gotSelection()) {	
			// stop playback
			stopPlaying();
			// crop vis
			samplerView.visView.cropImage(samplerView.imageView.getWidth());
			soundSampler.visData = ((DataBufferInt) samplerView.visView.getImage().getRaster().getDataBuffer()).getData();
			// crop image		
			samplerView.imageView.cropImage();
			// update width and duration
			width = samplerView.imageView.getWidth();
			duration = width / (double) sampleRate;
			vis = samplerView.visView.getImage();	
			// update sound sampler values
			soundSampler.visData = ((DataBufferInt) vis.getRaster().getDataBuffer()).getData();
			soundSampler.visHeight = vis.getHeight();	
			soundSampler.amplitudes = samplerView.imageView.getAmplitudes();
			soundSampler.imgWidth = width;
			soundSampler.imgHeight = samplerView.imageView.getHeight();
			soundSampler.remapFrequencies(samplerView.imageView.getSelection().y, samplerView.imageView.getSelection().height);		
			soundSampler.valueChanged = true;
					
			// set duration
			duration = width * soundSampler.samplesPerFrameNew / (float) sampleRate * 1000;
			
			// update sampler view
			samplerView.interfaceFrame.pack();
			samplerView.imageView.clearSelection();
		} else {
			System.out.println("no selection");
		}
	}
	
	// export samples to wav file
	public void export() {
		System.out.println("export");
		Float[] samples = soundSampler.getSamples();
		ExportFile export = new ExportFile(samples);
	}
}
