package visualSynth;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public final class SoundPlayer {
	
	private SourceDataLine lineOut;
	private int sampleRate;
	private int bufferSize;
	
	public SoundPlayer(int bSize, int rate) {
		bufferSize = bSize;
		sampleRate = rate;
		setupLine();
	}
	
	// start new thread
	public boolean write(byte[] out) {
		// check if line is running
		if(!lineOut.isRunning()) {
			setupLine();
		}
		
		if(lineOut.available() > out.length) {
			lineOut.write(out, 0, out.length);	
			/*
			if(lineOut.available() >= lineOut.getBufferSize()) {
				// TODO avoid buffer underflow
				System.out.println("Buffer underflow!!");
			}
			*/
			return true;
		} else {
			return false;
		}
	}
	
	private void setupLine() {
		// System.out.println("setup line..");
		// setup audio format
		// sampleRate, sampleSizeInBits, channels, signed, bigEndian
		AudioFormat audioFormat = new AudioFormat(sampleRate, 16, 1, true, false);
				
        DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat, bufferSize);       
        try {
			lineOut = (SourceDataLine) AudioSystem.getLine(info);	
			// open data line
			lineOut.open(audioFormat, bufferSize);		
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
        lineOut.start();
	}

	// returns size of current available buffer
	public int getBuffer() {
		return lineOut.available();
	}
	
	public void stop() {
		if(lineOut.isRunning()) {
			// flush
			lineOut.flush();
			// stop line out
			lineOut.close();
		}
	}
}
