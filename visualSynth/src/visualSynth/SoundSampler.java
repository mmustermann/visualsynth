package visualSynth;

import java.awt.Color;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;

public final class SoundSampler implements Runnable {
	
	private SoundController soundController;
		
	public int mode;
	public static int SIN = 0;
	public static int SAW = 1;
	public static int SQUARE = 2;

	public int[] visData;

	public int freqHigh;
	public int freqMid;
	public int freqLow;
	public float[] frequencies;
	
	public int imgWidth;
	public int imgHeight;
	public int visHeight;
		
	public int sampleRate;
	public int samplesPerFrameNew;
	private int samplesPerFrame;
	
	public float gain;
	public float volume;

	private boolean playing;
	public boolean loop;
	public boolean valueChanged;
	public boolean dynamicAmpControl;

	// ---------- delay effect ----------//
	public float decay;
	private int delayCounter;
	private int delayLength;
	public int delayLengthNew;
	private float[] delayLine;
	
	// ---------- smoothing effect ----------//	
	private int smoothLength;
	public int smoothLengthNew;
	private float[] averageValues;
	
	// ---------- dynamic range compression ----------//	
	// double gain = 0.25;
	double maxSampleValue = 2;
	
	// ---------- high pass ----------//	
	private static int NZEROS = 5;
	private static int NPOLES = 5;
	private double amplNorm[] = new double[NZEROS+1];
	private double ampOut[] = new double[NPOLES+1];

	private Thread thread;	

	private int[] valCounter;
	private float[][] sinValues;
	private float[][] sawValues;
	private float[][] squareValues;
	
	public float[] amplitudes;	
	
	private ArrayList<Float> lastSamples;
	private float[] samples;
	private float ramp;
	
	private static double twoPI = 2.0 * Math.PI;	
	
	private SoundPlayer soundplayer;

	public SoundSampler(SoundController soundCont){		
		soundController = soundCont;
	}
	
	public void init() {				
		// setup soundplayer
		soundplayer = new SoundPlayer(20000, sampleRate);
		// get frequencies
		frequencies = getFrequencies(freqHigh, freqLow);
	}
	
	// start new thread
	public void start() {
    	thread = new Thread(this);  
    	thread.setPriority(Thread.MAX_PRIORITY);
    	thread.start();
	}
	
	@Override
	public void run() {			
		playing = true;
		// clear the graph for drawing
		clearGraph();
		// generate samples and playback
		generateAndQueueSamples();		
		// wait until done playing
		while(soundplayer.getBuffer() < 10000) {
			try {
				thread.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
		// if loop run again
		if(loop) {
			run();
		} else {
			soundController.stopPlaying();
		}				
	}	
		
	// stop playback
	public void stop() {
		playing = false;
		loop = false;
		soundplayer.stop();
	}
	
	// retun all samples
	public Float[] getSamples() {
		return lastSamples.toArray(new Float[0]);
				
	}
	
	// generate samples and queue in sample player
	private void generateAndQueueSamples() {		
		// TODO update this in realtime?
		// get all values for frequencies
		// only need to update when pitch changed
		sinValues = getSinValues(frequencies, sampleRate);
		sawValues = getSawValues(frequencies, sampleRate);
		squareValues = getSquareValues(frequencies, sampleRate);	
			
		// store the complete samples from the last playback in this arrayList
		lastSamples = new ArrayList<Float>();	
		
		// keeps track of which values we used
		valCounter = new int[frequencies.length];
		//using this as a multiplier for amplitude -> fade in and out
		ramp = 0;
		
		// ---------- for debug ----------//
		double timeStart = System.currentTimeMillis();
		// ---------- for debug ----------//	
		
		// ---------- generate samples ----------//
		int sampleCounter = 0;
		double milisPerFrame = 0;
		for(int i = 0; i < imgWidth; i++) {		
			// update critical values per frame
			if(valueChanged) {	
				samplesPerFrame = samplesPerFrameNew;	
				delayLength = delayLengthNew;
				smoothLength = smoothLengthNew;
				delayCounter = 0;			
		
				// ---------- delay effect ----------//
				delayLine = new float[delayLength];		
				
				// ---------- smoothing effect ----------//
				averageValues = new float[smoothLength];
				
				valueChanged = false;
			}	
		
			double time;
			float sample;
			
			// fade in first 10 pixel to max of 1.0
			if(i < 10) {
				ramp += 0.1;
			// fade out last 10 pixel
			} else if (imgWidth - i < 11) {
				ramp -= 0.1;
			}	
			
			// array to store samples
			samples =  new float[samplesPerFrame];		
			// create samples for 1 pixel
			for(int f = 0; f < samplesPerFrame; f++) {
				time = (double) sampleCounter / (double) sampleRate;
				// get sample
				sample = generateSample(i, f, time);
				// apply delay effect
				if(delayLength > 0) sample = delayEffect(sample);			
				// apply smoothing effect
				if(smoothLength > 0) sample = smoothingEffect(sample);
				// limit signal and apply volume
				sample = compressRange(sample) * volume * ramp;
				
				samples[f] = sample;
				
				sampleCounter++;
			}	
			
			// apply a ~35 hz highpass
			highPass(samples);
			
			// just for test
			// add sample to last samples list for export
			for(float f : samples) {
				lastSamples.add(f);
			}

			// send samples to sound player
			// convert float samples to byte
			byte[] out = floatToByteArray(samples);
			
			// wait until data could be written			
			while(!soundplayer.write(out)) {
				try {
					// sleep 
					Thread.sleep(5);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}			
			// draw graph
			drawGraph(i);
			// stop
			if(!playing) break;
		}	
		// ---------- for debug ----------//
		double timeEnd = System.currentTimeMillis();
		milisPerFrame = timeEnd - timeStart;
		System.out.println("total time: " + milisPerFrame );
		System.out.println("avg time per sample: " + milisPerFrame / (samplesPerFrame * imgWidth));
		// ---------- for debug ----------//	
	}
	// generate a single sample combining all waves
	private float generateSample(int row, int sampleNum, double time) {
		// get amplitudes for this row
		float[] amplitudesRow = Arrays.copyOfRange(amplitudes, row * imgHeight, (row + 1) * imgHeight);
		// get amplitudes for next row		
		float[] amplitudesRowNext = new float[imgHeight];		
		if(row < imgWidth) amplitudesRowNext = Arrays.copyOfRange(amplitudes, (row +1) * imgHeight, (row + 2) * imgHeight);		
		
		float localTime;
		float ampThis;
		float ampNext;
		
		double sample = 0;
		float amplitude;
		// int row = counter * imgHeight;
		// amplitude threshold 		
		float amplitudeThreshold = 0.005f;				
		// add all signals for image height
		for(int i = 0; i < imgHeight; i++) {
			
			localTime = sampleNum / (float) samplesPerFrame;
			ampThis =  amplitudesRow[i];
			ampNext = amplitudesRowNext[i];
			// lerp amplitude
			if(ampThis > 0 || ampNext > 0) {
				amplitude = lerp(ampThis, ampNext, localTime);
			} else {
				amplitude = 0;
			}
			// add signal to sample
			if(amplitude > amplitudeThreshold) {
				if(mode == SIN) sample += sinValues[i][valCounter[i]] * amplitude;
				else if(mode == SAW) sample += sawValues[i][valCounter[i]] * amplitude;
				else if(mode == SQUARE) sample += squareValues[i][valCounter[i]] * amplitude;
				
				// if we used the value increase the val counter -> reset counter when max reached
				if (valCounter[i] < sinValues[i].length -1) valCounter[i] ++;
				else valCounter[i] = 0;
			} else {
				// reset counter when frequency not used
				valCounter[i] = 0;
			}
		}		
		return (float) sample * gain;
	}

	// apply delay effect to the sample
	private float delayEffect(float sample) {
		float decay = 0.6f;
		// apply delay effect to sample
		sample += delayLine[0];
		// shift array
		delayLine = Arrays.copyOfRange(delayLine, 1, delayLength + 1);
		//  add new value at last position
		delayLine[delayLength -1] = sample * decay;
		// return delay for current sample
		return sample;
	}
	
	// apply a smoothing effect
	private float smoothingEffect(float sample) {
		// shift array
		averageValues = Arrays.copyOfRange(averageValues, 1, smoothLength + 1);
		// set current value
		averageValues[smoothLength -1] = sample;	
		// ger average
		float average = 0;
		for(float avg : averageValues) {
			average += avg;
		}
		average /= smoothLength;
		// apply average
		return average;
	}
	
	// amplitude compressing
	// http://www.voegler.eu/pub/audio/digital-audio-mixing-and-normalization.html //
	private float compressRange(float sample) {		
		/*
		// double maxSampleValue = 2;
		double threshold = 0.6;
		double alpha = 7.48;	
		if(Math.abs(sample) > maxSampleValue) maxSampleValue = Math.abs(sample);	
		if(sample > threshold) {
			return (float) (threshold + (1 - threshold) * ((Math.log(1 + alpha * ((sample - threshold) / (maxSampleValue - threshold))) / Math.log(1 + alpha))));
		} else if (sample < threshold * -1) {
			return (float) (threshold + (1 - threshold) * ((Math.log(1 + alpha * ((Math.abs(sample) - threshold) / (maxSampleValue - threshold))) / Math.log(1 + alpha)))) * -1;			
		} else {
			return sample;
		}	
		*/	
		// optimized version
		if(Math.abs(sample) > maxSampleValue) maxSampleValue = Math.abs(sample);
		
		if(sample > 0.6) {
			return (float) (0.6 + 0.4 * ((Math.log(1 + 7.48 * ((sample - 0.6) / (maxSampleValue - 0.6))) / 2.1377104)));
		} else if (sample < -0.6) {
			return (float) (0.6 + 0.4 * ((Math.log(1 + 7.48 * ((Math.abs(sample) - 0.6) / (maxSampleValue - 0.6))) / 2.1377104))) * -1;			
		} else {
			return sample;
		}	
	}
	
	// ~35 hz high pass
	// http://www.velocityreviews.com/forums/t125258-high-pass-audio-filter-implemented-in-java.html //
	// http://jpkc.gnnu.cn/jpkc/Signal/xunishiyan/Java/IIRFiltDes102.html
	private void highPass(float data[]) {
	// coefficients a1, b1, b2 are inverted!!!!
	double a0 = 1;
	double a1 = 2;
	double a2 = 1;
	double b1 = 1.9888929;
	double b2 = -0.98895425;
		
	for (int i=0; i <data.length; ++i)	{
			amplNorm[0] = amplNorm[1];
			amplNorm[1] = amplNorm[2];
			amplNorm[2] = data[i];
			ampOut[0] = ampOut[1];
			ampOut[1] = ampOut[2];
			ampOut[2] = ((a0 * amplNorm[0]) + (a2 * amplNorm[2])) - (a1 * amplNorm[1]) + (b2 * ampOut[0]) + (b1 * ampOut[1]);
			data[i] = (float) ampOut[2];
		}
	}
	 
	private static float[][] getSinValues(float[] freqs, int sampleRate) {
		// 2D array to store values
		float[][] values = new float[freqs.length][];
		// time per sample
		double sampleTime = 1 / (double) sampleRate;	
		for(int i = 0; i < freqs.length; i++) {
			// counter to keep track of time
			double currentTime = 0d;
			// get current frequency
			float freq = freqs[i];
			// time for a complete wave
			double timePerWave = 1 / freq;
			// number of values to reproduce wave
			int samplesPerFreq = (int) (timePerWave / sampleTime);
			// array to store values
			values[i] = new float[samplesPerFreq];	
			// get values
			for(int f = 0; f < samplesPerFreq; f++) {
				values[i][f] = (float) generateSine(freq, currentTime);
				currentTime += sampleTime;
			}	
		}
		return values;
	}
	
	private static float[][] getSawValues(float[] freqs, int sampleRate) {
		// 2D array to store values
		float[][] values = new float[freqs.length][];
		// time per sample
		double sampleTime = 1 / (double) sampleRate;	
		for(int i = 0; i < freqs.length; i++) {
			// counter to keep track of time
			double currentTime = 0d;
			// get current frequency
			float freq = freqs[i];
			// time for a complete wave
			double timePerWave = 1 / freq;
			// number of values to reproduce wave
			int samplesPerFreq = (int) (timePerWave / sampleTime);
			// array to store values
			values[i] = new float[samplesPerFreq];	
			// get values
			for(int f = 0; f < samplesPerFreq; f++) {
				values[i][f] = (float) generateSaw(freq, currentTime);
				currentTime += sampleTime;
			}	
		}
		return values;
	}
	
	private static float[][] getSquareValues(float[] freqs, int sampleRate) {
		// 2D array to store values
		float[][] values = new float[freqs.length][];
		// time per sample
		double sampleTime = 1 / (double) sampleRate;	
		for(int i = 0; i < freqs.length; i++) {
			// counter to keep track of time
			double currentTime = 0d;
			// get current frequency
			float freq = freqs[i];
			// time for a complete wave
			double timePerWave = 1 / freq;
			// number of values to reproduce wave
			int samplesPerFreq = (int) (timePerWave / sampleTime);
			// array to store values
			values[i] = new float[samplesPerFreq];	
			// get values
			for(int f = 0; f < samplesPerFreq; f++) {
				values[i][f] = (float) generateSquare(freq, currentTime);
				currentTime += sampleTime;
			}	
		}
		return values;
	}
		
	// generate a sine value with given frequency and time
	private static double generateSine(float frequency, double time) {
        return Math.sin((frequency * twoPI * time));
	}
	
	// generate a sawtooth value with given frequency and time
	private static double generateSaw(float frequency, double time) {
		return 2 * (time * frequency - Math.floor(time * frequency)) -1;
	}
	
	// generate a square value with given frequency and time
	private static double generateSquare(float frequency, double time) {
		return (2 * Math.floor(time * frequency) - Math.floor(2 * time * frequency) + 0.5) * 2;
	}
	
	// mapping image height to frequency range with exponent
	public float[] getFrequencies(int high, int low) {
		float[] freq = new float[imgHeight];
		for(int i = 0; i < imgHeight; i++) {
			freq[imgHeight -1 - i] = (float) (((Math.pow(i, 2) / Math.pow(imgHeight, 2)) * (high - low)) + low);
		}
		return freq;
	}
	
	// remap frequencies to new height
	public void remapFrequencies(int start, int length) {
		float[] freqNew = new float[length];
		for(int i = 0; i < length; i++) {
			freqNew[i] = frequencies[start + i];
		}	
		frequencies =  freqNew;
	}	

	// draw graph for a row
	private void drawGraph(int x) {		
		int green = Color.GREEN.getRGB();	
		for(int i = 0; i < samples.length; i++) {	
			if(samples[i] <= 1 && samples[i] >= -1) {
				int y = (int) ((visHeight / 2) + (samples[i] * (visHeight / 2)));
				visData[y * imgWidth + x] = green;
			} else {
				// out of range
				System.out.println("signal clipping!");
			}
		}		
		soundController.updateVis();
	}
	
	// override graph with black color
	private void clearGraph() {
		int black = Color.BLACK.getRGB();		
		for(int i = 0; i < visData.length; i++) {
			visData[i] = black;
		}
		soundController.updateVis();
	}	
		
    // converts 32 bit float to 16 bit byte
    private static byte[] floatToByteArray(float[] values) {
        ByteBuffer buffer = ByteBuffer.allocate(2 * values.length);
        buffer.order(ByteOrder.LITTLE_ENDIAN);     
        for(int i = 0; i < values.length; i++) {  
        	short f = (short) (values[i] * 32767);
	        if( f > 32767 ) f = 32767;
	        if( f < -32767 ) f = -32767;
            buffer.putShort(f);        
        }
        return buffer.array();
    }
	
	// lerp between to values
	private static float lerp(float v1, float v2, float time) {
		return v1 + time * (v2 - v1);
	}
}
