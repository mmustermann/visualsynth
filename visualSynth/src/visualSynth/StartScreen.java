package visualSynth;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

public final class StartScreen implements ActionListener, AWTEventListener  {
	
	private JFrame interfaceFrame;	
	private JPanel panelSamplerEntries;
	
	private JButton btnOpenFile;
	private JButton btnNew;	
	private JToggleButton btnPlay;
	
	private FileLoader fLoader;	
	private Timeline timeline;

	private ArrayList<SamplerEntry> samplerEntries;
	
	public StartScreen(int w, int h) {		
		samplerEntries = new ArrayList<SamplerEntry>();	
		
		fLoader = new FileLoader();	
		timeline = new Timeline();
		
		// setup interface frame
		interfaceFrame = new JFrame("Visual Snyth v0.2b");
		interfaceFrame.setLayout(new java.awt.FlowLayout());
    	interfaceFrame.setResizable(false);    	
		interfaceFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
        // setup panel
    	JPanel panelNew = new JPanel();
    	panelNew.setLayout(new java.awt.FlowLayout());
    	panelNew.setPreferredSize(new Dimension(100, 150));
    	
        // sampler entry panel
    	panelSamplerEntries = new JPanel();
    	panelSamplerEntries.setLayout(new BoxLayout(panelSamplerEntries, BoxLayout.Y_AXIS));
		
		btnOpenFile = new JButton("Open file");
		btnOpenFile.setPreferredSize(new Dimension(100, 40));
		btnOpenFile.addActionListener(this);
		
		btnNew = new JButton("New");
		btnNew.setPreferredSize(new Dimension(100, 40));
		btnNew.addActionListener(this);
		
		btnPlay = new JToggleButton("Play");
		btnPlay.setPreferredSize(new Dimension(100, 40));
		btnPlay.addActionListener(this);
		
		// add stuff
    	interfaceFrame.add(panelNew);		
		panelNew.add(btnOpenFile);
		panelNew.add(btnNew);	
		panelNew.add(btnPlay);
		
		interfaceFrame.add(panelSamplerEntries);
		interfaceFrame.add(timeline);	
		
		// set visible
    	interfaceFrame.pack();
		interfaceFrame.setVisible(true);	
		
		// key listener
		interfaceFrame.getToolkit().addAWTEventListener(this, AWTEvent.KEY_EVENT_MASK);
	}

	// add a sampler entry to the startscreen
	private void addSamplerEntry(SamplerEntry samplerEntry) {
		samplerEntries.add(samplerEntry);
		// add to UI
		panelSamplerEntries.add(samplerEntry);
    	interfaceFrame.pack();
	}
	
	// remove entry from the startscreen and timeline
	public void removeEntry(SoundController sController) {	
		for(Component comp : panelSamplerEntries.getComponents()) {
			if(comp instanceof SamplerEntry) {
				SamplerEntry smplr = (SamplerEntry) comp;
				if(smplr.getSoundcontroller() == sController) {
					samplerEntries.remove(smplr);			
					panelSamplerEntries.remove(smplr);	
					// update sampler id`s
					for(int i = 0; i < samplerEntries.size(); i++) {
						samplerEntries.get(i).setId(i + 1);
					}
					// update timeline
					timeline.stop();
					timeline.removeSoundController(sController);
					interfaceFrame.validate();				
					break;
				}
			}
		}
	}
	
	// create new sampler entry
	private void newSampler(BufferedImage img) {
		// keep track of how many sampler we got
		int id = samplerEntries.size() + 1;
		// create new name
		String name = "Sampler";
    	addSamplerEntry(new SamplerEntry(id, name, img));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnOpenFile){
			fileDialog();
		}
		if(e.getSource() == btnNew){
			newSampler(new BufferedImage(800, 400, BufferedImage.TYPE_INT_RGB));
		}
		if(e.getSource() == btnPlay){
			if(!timeline.isRunning()) timeline.start();
			else timeline.stop();
		}
	}
	
	private void fileDialog()
	{
		final JFileChooser fc = new JFileChooser();
		FileFilter filter = new FileNameExtensionFilter("Image files", "jpg", "jpeg", "png", "gif");
		fc.addChoosableFileFilter(filter);
		fc.setAcceptAllFileFilterUsed(false);	
        int returnVal = fc.showOpenDialog(fc);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            BufferedImage img = fLoader.loadfile(file);
            if(img != null) {
    			newSampler(img);
            }
        }
	}
	
	@Override
	public void eventDispatched(AWTEvent event) {
		if(event instanceof KeyEvent){
			KeyEvent key = (KeyEvent)event;
			if(key.getID()==KeyEvent.KEY_PRESSED){ //Handle key presses
				// System.out.println(key.getKeyChar());
				int cValue = Character.getNumericValue(key.getKeyChar()) -1;
				// was number key pressed?
				if(cValue < 10 && cValue > -1) {					
					// do we have a sampler for that value?
					// TODO clean this up - super ugly code
					if(cValue < samplerEntries.size()) {
						SamplerEntry smplr = samplerEntries.get(cValue);
						smplr.getSoundcontroller().play();
						key.consume();
					}
				}		
			}
		}
	}
}
