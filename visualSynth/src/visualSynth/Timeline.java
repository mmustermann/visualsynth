package visualSynth;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JPanel;

public final class Timeline extends JPanel implements Runnable, DropTargetListener, MouseListener, MouseMotionListener  {

	private static final long serialVersionUID = -8525153956647032350L;
	// array list to store timeline entries
	private ArrayList<TimelineEntry> timelineEntries;
	private TimelineEntry entryActive; 
	
	private double timeIndexDuration;
	private int numOfTimeIndices;
	private int currentTimeIndex;
	
	private int width;
	private int height;
	private int samplerHeight;
	private double duration;
	private boolean running;	
	private Thread thread;
	
	JPanel panelSampler;
	
	public Timeline() {
		timelineEntries = new  ArrayList<TimelineEntry>();
		// length of the timeline in milliseconds
		duration = 10000;
		width = 800;
		height = 200;	
		samplerHeight = 40;
		// duration of a time index in milliseconds
		timeIndexDuration = 100;
		// number of indices
		numOfTimeIndices = (int) (duration / timeIndexDuration);							
		// for drag and drop
		new DropTarget(this, DnDConstants.ACTION_LINK, this, true, null);
		
		addMouseListener(this);	
		addMouseMotionListener(this);
	}
	
	private void addTimelineEntry(SoundController sController, int startIndex) {	
		// create a new entry for the controller
		TimelineEntry entry = new TimelineEntry(sController);		
		setStartIndex(entry, startIndex);
		timelineEntries.add(entry);
		// System.out.println("timeline entry added: " + sController.getName());	
	}
	
	private void removeTimelineEntry(TimelineEntry entryRe) {
		for(TimelineEntry entry : timelineEntries) {
			if(entry == entryRe) {
				timelineEntries.remove(entry);
				System.out.println("timeline entry removed: " + entry.getStartIndex());		
				repaint();
				break;				
			}
		}
	}
	
	public void removeSoundController(SoundController sController) {
		for(int i = 0; i < timelineEntries.size(); i++) {
			TimelineEntry entry = timelineEntries.get(i);
			if(entry.getSController() == sController) {
				timelineEntries.remove(i);
				i --;
				System.out.println("timeline entry removed: " + entry.getStartIndex());				
			}
		}
		repaint();	
	}
		
	// add a start index to soundcontroller
	private void setStartIndex(TimelineEntry entryNew, int startIndex) {		
		// check if we overlap with a entry of the same controller
		for(TimelineEntry entry : timelineEntries) {
			if(entry.getSController() == entryNew.getSController() && entry != entryNew) {
				int index = entry.getStartIndex();
			
				int indexLength = (int) (Math.round(entryNew.getSController().getDuration() / timeIndexDuration));		
				// check if start point is within 
				if(startIndex > index && startIndex < index + indexLength) startIndex = index + indexLength;
				// check if end point is within 
				else if(startIndex + indexLength > index && startIndex + indexLength < index + indexLength) startIndex = index - indexLength;
			}
		}				
		
		// check if start index in bounds
		if(startIndex < 0 || startIndex > numOfTimeIndices) {
			System.out.println("Start index out of range"); 
		} else {				
			System.out.println("timeline entry startIdex set: " + startIndex);			
			// calculate start point		
			int startPointX = startIndex * (width / numOfTimeIndices);
			// int startPointY = entryNew.getSController().getId() * samplerHeight;
			int startPointY = 1 * samplerHeight;
			
			// calculate length 
			int rectWidth = (int) ((entryNew.getSController().getDuration() * width) / duration);
			
			// get hitbox for this entry
			Rectangle rect = new Rectangle(startPointX, startPointY, rectWidth, samplerHeight);
			
			entryNew.setStartIndex(startIndex);		
			entryNew.setHitbox(rect);
			
			repaint();
		}
	}
	
	// start playback of the timeline
	public void start() {
		running = true;		
    	thread = new Thread(this);  
    	thread.setPriority(Thread.NORM_PRIORITY);
    	thread.start();
	}
	
	// stop playback of the timeline
	public void stop() {
		running = false;
		currentTimeIndex = 0;
		repaint();
	}
	
	public boolean isRunning() {
		return running;
	}
	
	@Override
	public void run() {
		double startTime = System.currentTimeMillis();
		currentTimeIndex = 0;
		
		while(running) {
			// we loop all sound controller and get the starttimes
			for(TimelineEntry entry : timelineEntries) {
				// check if start index matches current index
				if(entry.getStartIndex() == currentTimeIndex) {
					// start the sound controller
					entry.getSController().play();
				}		
			}	 		
			// sleep until we are at next time index
			while(System.currentTimeMillis() - startTime < 100) {				
				try {
					thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			// add to the current index - reset when we reach end
			if(currentTimeIndex < numOfTimeIndices) currentTimeIndex++;
			else currentTimeIndex = 0;
			
			repaint();
			// update start time
			startTime = System.currentTimeMillis();
		}		
	}

	@Override
	public void paintComponent(Graphics g) {		
	    g.setColor(Color.BLACK);   
		g.fillRect(0, 0, width, height);
		
	    Color col = new Color(0, 255, 0, 50);
	    g.setColor(col);   
	    
	    for(int i = 0; i < 10; i++) {
		    g.drawLine((int) ((width / 10) * i), 0, (int) ((width / 10) * i), height);
	    }
	    
	    g.drawLine(0, (int) (height * 0.5), width, (int) (height * 0.5));
	    
	    // draw current position
	    g.setColor(Color.RED);
	    int currentPos = currentTimeIndex * (width / numOfTimeIndices);    
	    g.drawLine(currentPos, 0, currentPos, height);	    
	    
		for(TimelineEntry entry : timelineEntries) {
			// get hitbox
			Rectangle rect = entry.getHitbox();
			
			// draw hitbox
		    g.setColor(Color.GREEN);
			g.fillRect(rect.x, rect.y, rect.width, rect.height);
			
			// draw scaled version of vis image
			g.drawImage(entry.getSController().getVis(), rect.x + 1, rect.y + 1, rect.width - 2, rect.height - 2, null);
			
			// draw name
		    // g.setColor(Color.WHITE);				
			// g.drawString(sController.getName(), startPoint + 10, (40 * i) + 10);	
		}	    
	}
	
	@Override	
	public Dimension getPreferredSize() {
		return new Dimension(width, height);	
	}

	@Override	
	public Dimension getMinimumSize() {
		return new Dimension(width, height);	
	}

	@Override
	public void dragEnter(DropTargetDragEvent dtde) {
		// System.out.println("dragEnter");
	}

	@Override
	public void dragOver(DropTargetDragEvent dtde) {
		// System.out.println("dragOver");		
	}

	@Override
	public void dropActionChanged(DropTargetDragEvent dtde) {
		// System.out.println("dropActionChanged");		
	}

	@Override
	public void dragExit(DropTargetEvent dte) {
		// System.out.println("dragExit");		
	}

	@Override
	public void drop(DropTargetDropEvent event) {
		// TODO Auto-generated method stub
		// System.out.println("drop");		
		Transferable trans = event.getTransferable();
		if (event.isDataFlavorSupported(new DataFlavor(SoundController.class, "A SamplerEntry Object"))) {
			event.acceptDrop(event.getDropAction()); 	  
			try {
				SoundController sController = (SoundController) trans.getTransferData(new DataFlavor(SoundController.class, "A SamplerEntry Object"));	
				// calculate start index			
				Point point = event.getLocation();
				int startIndex = (int) (point.x / (float) (width / numOfTimeIndices));
				// create new timeline entry
				addTimelineEntry(sController, startIndex);				
			} catch (UnsupportedFlavorException e) {
			} catch (IOException e) {
			}   	  
			event.dropComplete(true);
		} else {
			event.rejectDrop();
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if(entryActive != null) {
			// TODO move rectangle
		}	
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		// check if we hit a entry
		for(TimelineEntry entry : timelineEntries) {
			if(entry.getHitbox().contains(e.getPoint())) {
				System.out.println("hit entry: " + entry.getStartIndex());
				// start dragging
				entryActive = entry;
				break;
			}
		}	
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// set new position
		if(entryActive != null) {
			// calculate start index			
			Point point = e.getPoint();
			int startIndex = (int) (point.x / (float) (width / numOfTimeIndices));
			
			setStartIndex(entryActive, startIndex);
			entryActive = null;
		}		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if(entryActive != null) {
			// TODO delete entry
			removeTimelineEntry(entryActive);
			entryActive = null;
		}	
	}
}