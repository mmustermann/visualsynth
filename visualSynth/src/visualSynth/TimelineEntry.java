package visualSynth;

import java.awt.Rectangle;

public class TimelineEntry {
	
	private SoundController soundController;
	
	private int startIndex;
	private Rectangle hitbox;
	
	public TimelineEntry(SoundController sController) {
		
		soundController = sController;
	}
	
	// return the sound controller 
	public SoundController getSController() {
		return soundController;
	}
	
	// set start index
	public void setStartIndex(int index) {
		startIndex = index;
	}
	
	// get start index
	public int getStartIndex() {
		return startIndex;
	}
	
	// set hitbox
	public void setHitbox(Rectangle rect) {
		hitbox = rect;
	}

	// get Hitbox
	public Rectangle getHitbox() {
		return hitbox;
	}
}
