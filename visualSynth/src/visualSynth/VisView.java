package visualSynth;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JComponent;

public class VisView extends JComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3909833080793564170L;
	private BufferedImage image;
	private int width;
	private int height;
	
	public VisView(int w, int h) {
		width = w;
		height = h;
		image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		setBounds(0, 0, width, height);
	}
	
	public BufferedImage getImage() {
		return image;
	}

	public void update() {
		repaint();
	}
	
	public void clear() {
		image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		repaint();
	}
	
	public void cropImage(int w) {
		width = w;
		image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);	
		setBounds(0, 0, width, height);
		repaint();
	}
	
	@Override
	public void paintComponent(Graphics g) {
		setForeground(Color.BLACK);
	    g.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
	    g.setColor(Color.RED);
	    g.drawLine(0, 10, width, 10);
	    g.drawLine(0, height - 10, width, height - 10);
	}
	
	@Override	
	public Dimension getPreferredSize() {
		return new Dimension(width, height);		
	}

	@Override	
	public Dimension getMinimumSize() {
		return new Dimension(width, height);	
	}
}
